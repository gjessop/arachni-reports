#!/bin/sh

# echo "file found: $1"
NOW=$(date +"%Y-%m-%d")
unzip -d "/home/zymewire/Downloads/arachni-reports/full/$NOW" "/home/zymewire/Downloads/arachni-reports/zipped/$1" && rm -f "/home/zymewire/Downloads/arachni-reports/zipped/$1"
export DISPLAY=:0 && google-chrome --disable-gpu "/home/zymewire/Downloads/arachni-reports/full/$NOW/index.html"
