# README #

Arachni sends a report every time it is run.

It is currently running via a cronjob on test.zymewire.com

The run is setup via a shell script found at /opt/arachni/bin/arachni_cron.sh

When run, it emails a report to the address found in that setup script.

To view that report, ensure you have Chrome (or edit unzip.sh to reflect your browser) and do the following:

1. install arachni on your machine: http://www.arachni-scanner.com/download/

    > `this repo expect arachni to be stored in /opt/arachni`
    
1. install this repo into your Downloads folder (you can install it elsewhere but installing it in Downloads makes it easy to drop the downloaded report on this repo's folder structure)
1. install incron 
	> `sudo apt-get install incron`
1. allow your user (in this example user is zymewire) to use incron by editing incron.allow and entering the user name
	> `sudo nano /etc/incron.allow`
1. edit the incron configuration to have it start watching the folders from this repo:
	> `incrontab -e`
	>
	> `/home/zymewire/Downloads/arachni-reports/zipped IN_CLOSE_WRITE,IN_MOVED_TO /home/zymewire/Downloads/arachni-reports/unzip.sh $#`
	>
	> `/home/zymewire/Downloads/arachni-reports IN_MOVED_TO /home/zymewire/Downloads/arachni-reports/convert.sh $#`
1. when a report is emailed to you, download it and drop it onto this repo's folder (arachni-reports).  This will convert the AFR report into an HTML report and fire up a chrome browser.  It may take a few seconds depending on the size of the report.


